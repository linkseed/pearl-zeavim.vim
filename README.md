# zeavim.vim for Pearl

Zeavim, [Zeal](https://zealdocs.org/) for Vim

## Details

- Plugin: https://github.com/KabbAmine/zeavim.vim
- Pearl: https://github.com/pearl-core/pearl
